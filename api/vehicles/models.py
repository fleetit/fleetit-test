from django.db import models

# Create your models here.
class Vehicle(models.Model):
    make = models.CharField(max_length=128)
    model = models.CharField(max_length=128)
    year = models.IntegerField()
    plate_number = models.CharField(max_length=32)