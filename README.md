![FleetIT Logo](https://fleetit.com/_next/static/media/fleetit-logo.fbad3e68.svg)

# Software Engineer Hiring Challenge

Welcome to this repository for our Software Engineer Hiring Process. In this challenge, you will enhance an existing Django-based CRUD application for managing vehicles and drivers. 

## Challenge Overview

### Requirements

1. **Adding Tagging Feature:**
   - Modify the existing Django application provided in the `api` folder to include the ability to add tags to vehicles and drivers.
   - Create a new database model for tags and establish a relationship with the vehicle and driver models.

2. **Vehicle-Driver Relationship:**
   - Enhance the application to establish a relationship between vehicles and drivers.
   - A driver can drive multiple vehicles.
   - A vehicle can have multiple drivers.
   - Record when a vehicle was driven by a driver.

3. **API Endpoints:**
   - Create API endpoints to perform the following actions:
     - Create a new tag.
     - List all tags.
     - Add one or more tags to a vehicle.
     - Add one or more tags to a driver.
     - List all tags associated with a vehicle or driver.
     - Search for vehicles by tag.
     - Search for drivers by tag.

4. **Bonus Points (Optional):**
   - Document your API thoroughly, explaining how to use the endpoints and what responses to expect.
   - Write unit tests to validate the correctness of your code and include a test suite.
   - Consider providing Docker support with a Dockerfile and instructions for running the application in a Docker container.

5. **Submission:**
   - Commit your changes to your GitLab repository.
   - Provide us with a link to your repository when you're ready for review.

### Evaluation Criteria

Your submission will be evaluated based on the following criteria:

- **Code Quality:** We will assess the cleanliness, organization, and readability of your code.

- **Functionality:** The application should allow CRUD operations for vehicles, drivers, and tags. It should also handle the relationships between vehicles, drivers, and tags correctly, including recording when a vehicle was driven by a driver.

- **Extensibility:** We will consider whether your code is designed in a way that makes it easy to extend and add new features in the future.

- **Bonus Points (Optional):**
   - **Documentation:** Clear and thorough API documentation will be appreciated.
   - **Testing:** Well-written unit tests will contribute to your evaluation.

## Getting Started

To get started with this challenge, follow these steps:

1. Fork this repository.

2. Review the existing Django application for managing vehicles and drivers in the `api` folder.

3. Implement the required functionality to add tags to vehicles, drivers, and establish the relationships between them, along with the corresponding API endpoints as described in the "Requirements" section above.

4. If you choose to earn bonus points, document your API comprehensively, write unit tests for your code, and consider providing Docker support.

5. Commit your changes to your GitLab repository.

6. Share the link to your GitLab repository with us for review.

## Questions and Support

If you have any questions or need support during the challenge, please don't hesitate to reach out.
